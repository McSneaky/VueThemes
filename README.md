# client-vue
> A Vue.js project

>Vue component base themes and page structure from JSON :)

>Themes can have different components and will fall back to default theme component, when given theme component loading fails.

>Pages and components are lazy loaded (with fallback to default).

>Page structure comes in from JSON (Just an example)

TODO:

- [ ] Rename things, components or widgets?
- [ ] Make ThemeLoader seperate plugin or mixin?
- [ ] Make WidgetLoader seperate plugin or mixin?
- [ ] Move themes to config
- [ ] Add tests

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
