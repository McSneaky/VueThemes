import Vue from 'vue'
import Router from 'vue-router'
import ThemeLoader from '@/extensions/ThemeLoader'

const Hello = () => import('@/components/Hello')

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello
    },
    {
      path: '/world',
      name: 'World',
      component: ThemeLoader.load('World', 'theme1')
    },
    {
      path: '/test',
      name: 'TestPage',
      component: ThemeLoader.load('TestPage'),
      props: {
        json: require('@/testData.json')
      }
    },
    {
      path: '/test123',
      name: 'TestPage123',
      component: ThemeLoader.load('TestPage123', process.env.THEME),
      props: {
        json: require('@/testData.json')
      }
    }

  ]
})
