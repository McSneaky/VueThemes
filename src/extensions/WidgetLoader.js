/**
 * Class for widget loader.
 *
 * @class      WidgetLoader
 */
export default class WidgetLoader {
  /**
   * Load required widget from required theme
   *
   * @param      {string}  widget  The widget
   * @param      {string}  theme   The theme
   * @return     {Promise}
   */
  static load (widget, theme = process.env.THEME_DEFAULT) {
    return () => import('@/themes/' + theme + '/widgets/' + widget)
      .catch(() => {
        return import('@/themes/' + process.env.THEME_DEFAULT + '/' + widget)
      })
  }

  /**
   * Loads multiple widgets from given theme
   *
   * @param      {array}   widgets  The widgets
   * @param      {string}  theme    The theme
   * @return     {object}           Object full Promises
   */
  static loadWidgets (widgets, theme = process.env.THEME_DEFAULT) {
    var components = {}
    for (let i in widgets) {
      components[widgets[i].type] = WidgetLoader.load(widgets[i].type, theme)
    }
    return components
  }
}
