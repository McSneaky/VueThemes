/**
 * Class for theme loader.
 *
 * @class      ThemeLoader (name)
 */
export default class ThemeLoader {
  /**
   * Load wanted component from (most) wanted theme
   *
   * @param      {string}  component  The component
   * @param      {string}  theme      The theme
   * @return     {Promise}
   */
  static load (component, theme = process.env.THEME_DEFAULT) {
    return () => import('@/themes/' + theme + '/' + component)
      .catch(() => {
        return import('@/themes/' + process.env.THEME_DEFAULT + '/' + component)
      })
  }
}
